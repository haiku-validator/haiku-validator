from django.conf.urls.defaults import *

urlpatterns = patterns("haikuvalidate.views",
    url(r"^$", "home"),
    url(r"^validate$", "validate"),
)
