from nltk.corpus import cmudict
from syllable import count as fallback_count
import string

class Word(object):
    d = cmudict.dict()
    known_counts = {}
    def __init__(self, word):
        self.original_word = word
        self.word = word.lower().strip(string.punctuation + string.digits)
        if not self.word:
            self.syllables = 0
            self.guessed = False
            return
        self.count()
        self.display_syllables = str(list(self.syllables))

    def count(self):
        # lookup known values
        if self.word in self.known_counts:
            self.syllables, self.guessed = self.known_counts[self.word]
            return
        try:
            # try pronunciation dictionary
            self.syllables = set(len([i for i in x if i[-1] in string.digits]) for x in self.d[self.word])
        except KeyError:
            # use fallback guessing routine
            self.syllables = set([fallback_count(self.word)])
            self.guessed = True
        else:
            self.guessed = False
        # update known values
        self.known_counts[self.word] = self.syllables, self.guessed

    def __nonzero__(self):
        if self.syllables:
            if any(self.syllables):
                return True
        return False

    def __str__(self):
        return self.original_word


class Haiku(object):
    def __init__(self, haiku):
        self.words = []
        c = 0
        for i in haiku.split():
            word = Word(i)
            if word:
                c += 1
                if c > 17:
                    # sanity check for long input
                    self.valid = False
                    return
                self.words.append(word)

        # get all possible arrangements of syllables
        arrangements = [[]]
        for word in self.words:
            arrangements = [i + [j] for j in word.syllables for i in arrangements]
        self.all_arrangements = arrangements

        # drop anything which isn't 17 syllables in total
        arr_total = []
        for i in arrangements:
            if sum(i) == 17:
                arr_total.append(i)
        if not arr_total:
            self.valid = False
            return

        # now drop anything which can't be split into 5,7,5
        arr_valid = []
        for arr in arr_total:
            scount = 0
            wcount = 0
            blocks = [5,7,5]
            words = []
            for word in arr:
                scount += word
                wcount += 1
                if scount == blocks[-1]:
                    words.append(wcount)
                    blocks.pop()
                    if not blocks:
                        arr_valid.append(words)
                        break
                    scount = 0
                elif scount > blocks[-1]:
                    break
        if not arr_valid:
            self.valid = False
            return

        self.valid = True
        self.arrangements = arr_valid
        self.guessed = any(i.guessed for i in self.words)

    def arrange(self, arrangement=0, short=False):
        if not self.valid:
            return None

        if short: join = " / "
        else: join = "\n"

        arr = self.arrangements[arrangement]

        lines = self.words[:arr[0]], self.words[arr[0]:arr[1]], self.words[arr[1]:]
        return join.join(" ".join(str(j) for j in i) for i in lines)

    def __nonzero__(self):
        return self.valid
