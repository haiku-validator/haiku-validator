from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.core.urlresolvers import reverse
import haiku_validator

def home(request):
    return render_to_response("haikuvalidate/home.html")

def validate(request):
    try:
        text = request.GET["haikutext"]
    except KeyError:
        return render_to_response("haikuvalidate/home.html")
    text = text.replace(u"\u2019", "'")
    haiku = haiku_validator.Haiku(text)
    return render_to_response("haikuvalidate/validate.html", {"haiku":haiku})
